# tree
Implementation of tree datastructure, including unit-tests proving program validity.

Three recursive tree traversal functions are present (in-order, pre-order and post-order).

Mostly sourced from: https://www.programiz.com/dsa/binary-tree

# Installation
1. Install ThrowTheSwitch "Unity" test framework.
2. Clone project
3. Navigate to project root
5. Build:
```shell
cmake -S ./ -B build-dir
cd build-dir
make
```

Generated binary "tree_test" in "build-dir/bin" runs unit-tests.
