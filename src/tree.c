#include <stdio.h>
#include <malloc.h>
#include "../include/tree.h"

#ifdef TEST
#define FRPINTF fprintf
#else
#define FPRINTF test_fprintf_tree
#endif

int (*test_fprintf_tree)(FILE *stream, const char *format, ...) = fprintf;

node *createNode(int nodeVal) {
    node *n = (node *) malloc(sizeof(node));

    n->data = nodeVal;
    n->left = NULL;
    n->right = NULL;

    return n;
}

int insertLeft(node *n, int nodeVal, node **insertedNode) {
    if (n->left != NULL) {
        FPRINTF(stderr, "Cannot insert to internal node.\n");
        return -1;
    }

    *insertedNode = createNode(nodeVal);
    n->left = *insertedNode;

    return 0;
}

int insertRight(node *n, int nodeVal, node **insertedNode) {
    if (n->right != NULL) {
        FPRINTF(stderr, "Cannot insert to internal node.\n");
        return -1;
    }

    *insertedNode = createNode(nodeVal);
    n->right = *insertedNode;

    return 0;
}

// Recursive function using "i" as index for appending to "inOrderArr".
int traverseInOrder(node *n, int inOrderArr[], int i) {
    if (n == NULL) return i;

    i = traverseInOrder(n->left, inOrderArr, i);

    inOrderArr[i] = n->data;
    i++;

    i = traverseInOrder(n->right, inOrderArr, i);
    return i;
}

// Recursive function using "i" as index for appending to "preOrderArr".
int traversePreOrder(node *n, int preOrderArr[], int i) {
    if (n == NULL) return i;

    preOrderArr[i] = n->data;
    i++;

    i = traversePreOrder(n->left, preOrderArr, i);
    i = traversePreOrder(n->right, preOrderArr, i);
    return i;
}

// Recursive function using "i" as index for appending to "postOrderArr".
int traversePostOrder(node *n, int postOrderArr[], int i) {
    if (n == NULL) return i;

    i = traversePostOrder(n->left, postOrderArr, i);
    i = traversePostOrder(n->right, postOrderArr, i);

    postOrderArr[i] = n->data;
    i++;

    return i;
}
