//
// Created by jurrew on 10/21/21.
//

#ifndef TREE_TREE_H
#define TREE_TREE_H

extern int (*test_fprintf_tree)(FILE *stream, const char *format, ...);

// Linked list isn't related to tree data structure, but used for storing
// tree traversals.
struct intList {
    int data;
    struct intList *next;
};
typedef struct intList il;

struct node {
    int data;
    struct node *left;
    struct node *right;
};
typedef struct node node;

node *createNode(int nodeVal);

int insertLeft(node *n, int nodeVal, node **insertedNode);

int insertRight(node *n, int nodeVal, node **insertedNode);

int traverseInOrder(node *n, int inOrderArr[], int i);

int traversePreOrder(node *n, int preOrderArr[], int i);

int traversePostOrder(node *n, int postOrderArr[], int i);

#endif //TREE_TREE_H
