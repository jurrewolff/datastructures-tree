//
// Created by jurrew on 10/20/21.
//

#include <unity/unity.h>
#include <malloc.h>
#include "../include/tree.h"

int mock_fprintf_tree(FILE *stream, const char *format, ...) {
    return 0;
}

void setUp(void) {
    test_fprintf_tree = mock_fprintf_tree; // Suppress expected err messages
}

void tearDown(void) {}

void freeTree(node *n) {
    if (n == NULL) return;

    freeTree(n->left);
    freeTree(n->right);

    free(n);
}

void test_createNode_should_CreateNodeWithValue(void) {
    int nodeVal = 5;
    node *root = createNode(nodeVal);

    TEST_ASSERT_EQUAL_INT(nodeVal, root->data);
    TEST_ASSERT_NULL(root->left);
    TEST_ASSERT_NULL(root->right);

    free(root);
    root = NULL;
}

void test_insertLeft_should_InsertNodeLeft(void) {
    int err;
    int rootNodeVal = 1;
    int leftNodeVal = 2;

    node *nLeft;
    node *root = (node *) malloc(sizeof(node));

    root->data = rootNodeVal;
    root->left = NULL;
    root->right = NULL;

    err = insertLeft(root, leftNodeVal, &nLeft);
    TEST_ASSERT_EQUAL_INT(0, err);

    TEST_ASSERT_EQUAL_INT(rootNodeVal, root->data);
    TEST_ASSERT_NOT_NULL(root->left);
    TEST_ASSERT_NULL(root->right);

    TEST_ASSERT_EQUAL_INT(leftNodeVal, nLeft->data);
    TEST_ASSERT_NULL(nLeft->left);
    TEST_ASSERT_NULL(nLeft->right);

    free(nLeft);
    free(root);
    nLeft = NULL;
    root = NULL;
}

void test_insertRight_should_InsertNodeRight(void) {
    int err;
    int rootNodeVal = 1;
    int rightNodeVal = 2;

    node *nRight;
    node *root = (node *) malloc(sizeof(node));

    root->data = rootNodeVal;
    root->left = NULL;
    root->right = NULL;

    err = insertRight(root, rightNodeVal, &nRight);
    TEST_ASSERT_EQUAL_INT(0, err);

    TEST_ASSERT_EQUAL_INT(rootNodeVal, root->data);
    TEST_ASSERT_NOT_NULL(root->right);
    TEST_ASSERT_NULL(root->left);

    TEST_ASSERT_EQUAL_INT(rightNodeVal, nRight->data);
    TEST_ASSERT_NULL(nRight->left);
    TEST_ASSERT_NULL(nRight->right);

    free(nRight);
    free(root);
    nRight = NULL;
    root = NULL;
}

void test_insertRight_should_not_InsertToInternalNode(void) {
    int err;
    int rootNodeVal = 1;
    int rightNodeVal = 2;
    int invalidRightNodeVal = 3;

    node *nRight = (node *) malloc(sizeof(node));
    node *root = (node *) malloc(sizeof(node));

    nRight->data = rightNodeVal;
    nRight->left = NULL;
    nRight->right = NULL;
    root->data = rootNodeVal;
    root->left = NULL;
    root->right = nRight;

    err = insertRight(root, invalidRightNodeVal, &nRight);
    TEST_ASSERT_EQUAL_INT(-1, err);

    TEST_ASSERT_EQUAL_INT(rootNodeVal, root->data);
    TEST_ASSERT_NOT_NULL(root->right);
    TEST_ASSERT_NULL(root->left);

    TEST_ASSERT_EQUAL_INT(rightNodeVal, nRight->data);
    TEST_ASSERT_NULL(nRight->left);
    TEST_ASSERT_NULL(nRight->right);

    free(nRight);
    free(root);
    nRight = NULL;
    root = NULL;
}

void test_insertLeft_should_not_InsertToInternalNode(void) {
    int err;
    int rootNodeVal = 1;
    int leftNodeVal = 2;
    int invalidLeftNodeVal = 3;

    node *nLeft = (node *) malloc(sizeof(node));
    node *root = (node *) malloc(sizeof(node));

    nLeft->data = leftNodeVal;
    nLeft->left = NULL;
    nLeft->right = NULL;
    root->data = rootNodeVal;
    root->left = nLeft;
    root->right = NULL;

    err = insertLeft(root, invalidLeftNodeVal, &nLeft);
    TEST_ASSERT_EQUAL_INT(-1, err);

    TEST_ASSERT_EQUAL_INT(rootNodeVal, root->data);
    TEST_ASSERT_NOT_NULL(root->left);
    TEST_ASSERT_NULL(root->right);

    TEST_ASSERT_EQUAL_INT(leftNodeVal, nLeft->data);
    TEST_ASSERT_NULL(nLeft->left);
    TEST_ASSERT_NULL(nLeft->right);

    free(nLeft);
    free(root);
    nLeft = NULL;
    root = NULL;
}

void test_traverseInorder_should_ReturnInorderList(void) {
    int rootNodeVal = 1;
    int rootLeft = 12;
    int rootLeftLeft = 5;
    int rootLeftRight = 6;
    int rootRight = 9;

    int expected[5] = {rootLeftLeft, rootLeft, rootLeftRight, rootNodeVal, rootRight};

    node *root = createNode(rootNodeVal);

    root->left = createNode(rootLeft);
    root->left->left = createNode(rootLeftLeft);
    root->left->right = createNode(rootLeftRight);
    root->right = createNode(rootRight);

    int *traversalList = calloc(5, sizeof(int));
    int listSize = traverseInOrder(root, traversalList, 0);

    TEST_ASSERT_EQUAL_INT(5, listSize);

    for (int i = 0; i < listSize; ++i) {
        TEST_ASSERT_EQUAL_INT(expected[i], traversalList[i]);
    }

    freeTree(root);
    free(traversalList);
    traversalList = NULL;
}

void test_traversePreOrder_should_ReturnPreOrderList(void) {
    int rootNodeVal = 1;
    int rootLeft = 12;
    int rootLeftLeft = 5;
    int rootLeftRight = 6;
    int rootRight = 9;

    int expected[5] = {rootNodeVal, rootLeft, rootLeftLeft, rootLeftRight, rootRight};

    node *root = createNode(rootNodeVal);

    root->left = createNode(rootLeft);
    root->left->left = createNode(rootLeftLeft);
    root->left->right = createNode(rootLeftRight);
    root->right = createNode(rootRight);

    int *traversalList = calloc(5, sizeof(int));
    int listSize = traversePreOrder(root, traversalList, 0);

    TEST_ASSERT_EQUAL_INT(5, listSize);

    for (int i = 0; i < listSize; ++i) {
        TEST_ASSERT_EQUAL_INT(expected[i], traversalList[i]);
    }

    freeTree(root);
    free(traversalList);
    traversalList = NULL;
}

void test_traversePostOrder_should_ReturnPostOrderList(void) {
    int rootNodeVal = 1;
    int rootLeft = 12;
    int rootLeftLeft = 5;
    int rootLeftRight = 6;
    int rootRight = 9;

    int expected[5] = {rootLeftLeft, rootLeftRight, rootLeft, rootRight, rootNodeVal};

    node *root = createNode(rootNodeVal);

    root->left = createNode(rootLeft);
    root->left->left = createNode(rootLeftLeft);
    root->left->right = createNode(rootLeftRight);
    root->right = createNode(rootRight);

    int *traversalList = calloc(5, sizeof(int));
    int listSize = traversePostOrder(root, traversalList, 0);

    TEST_ASSERT_EQUAL_INT(5, listSize);

    for (int i = 0; i < listSize; ++i) {
        TEST_ASSERT_EQUAL_INT(expected[i], traversalList[i]);
    }

    freeTree(root);
    free(traversalList);
    traversalList = NULL;
}

int main(void) {
    UNITY_BEGIN();

    RUN_TEST(test_createNode_should_CreateNodeWithValue);

    RUN_TEST(test_insertLeft_should_InsertNodeLeft);
    RUN_TEST(test_insertRight_should_InsertNodeRight);
    RUN_TEST(test_insertRight_should_not_InsertToInternalNode);
    RUN_TEST(test_insertLeft_should_not_InsertToInternalNode);

    RUN_TEST(test_traverseInorder_should_ReturnInorderList);
    RUN_TEST(test_traversePreOrder_should_ReturnPreOrderList);
    RUN_TEST(test_traversePostOrder_should_ReturnPostOrderList);

    return UNITY_END();
}
